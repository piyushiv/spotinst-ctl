from setuptools import setup, find_packages

with open('requirements.txt') as f:
	requirements = f.readlines()
with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
		name ='shivctl',
		version ='1.0.0',
		author ='shiv',
		author_email ='shiveve.2013@gmail.com',
		url ='https://gitlab.com/piyushiv/spotinst-ctl',
		description ='Demo Package for spotint',
		long_description = long_description,
		long_description_content_type ="text/markdown",
		license ='MIT',
		packages = find_packages(),
		scripts=['shivctl'],
		classifiers =(
			"Programming Language :: Python :: 3",
			"License :: OSI Approved :: MIT License",
			"Operating System :: OS Independent",
		),
		keywords ='shivctl',
		install_requires = requirements,
		zip_safe = False
)
